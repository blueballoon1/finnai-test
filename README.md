## Part 1:  
	Configuration:  
		Install Python 3  
		Install selenium package using pip  
		Download chromedriver and save to <work_directory>/chromedriver_win32/  
	Files:  
		Script:   
			seleniumTest.py  
		Optional user input file:   
			userSearch.txt 
		Output folder:   
			<work_directory>/output/YYYY-mm-dd-hhmmss/   
	Run:   
		Without user input:   
			python seleniumTest.py   
		With user input:  
			python seleniumTest.py -f userSearch.txt   
		For more information:  
			python seleniumTest.py -h  

## Part 2:   
	CI_CD_template.txt
	
## Part 3:
	Requirement:
		dockerFile, seleniumTest.py and userSearch.txt in the same working directory
	Run:
		docker build -t test .  
		docker run test -f userSearch.txt  
	
	